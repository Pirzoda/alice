# Alice's Adventures in Wonderland

![Alice's Adventures in Wonderland][1]

## ALICE'S ADVENTURES IN WONDERLAND

### BY LEWIS CARROLL

*WITH FORTY-TWO ILLUSTRATIONS BY JOHN TENNIEL*  
VolumeOne Publishing  
Chicago, Illinois 1998

[A BookVirtual Digital Edition, v1.2][2]
November, 2000
![Alice's Adventures in Wonderland][3]

```md
All in the golden afternoon
  Full leisurely we glide;
For both our oars, with little skill,
  By little arms are plied,
While little hands make vain pretence
  Our wanderings to guide.

Ah, cruel Three! In such an hour,
  Beneath such dreamy weather,
To beg a tale of breath too weak
  To stir the tiniest feather!
Yet what can one poor voice avail
  Against three tongues together?

Imperious Prima flashes forth
  Her edict 'to begin it' –
In gentler tone Secunda hopes
  'There will be nonsense in it!' –
While Tertia interrupts the tale
  Not more than once a minute.

Anon, to sudden silence won,
  In fancy they pursue
The dream-child moving through a land
  Of wonders wild and new,
In friendly chat with bird or beast –
  And half believe it true.

And ever, as the story drained
  The wells of fancy dry,
And faintly strove that weary one
  To put the subject by,
"The rest next time -" "It is next time!"
  The happy voices cry.

Thus grew the tale of Wonderland:
  Thus slowly, one by one,
Its quaint events were hammered out –
  And now the tale is done,
And home we steer, a merry crew,
  Beneath the setting sun.

Alice! a childish story take,
  And with a gentle hand
Lay it were Childhood's dreams are twined
  In Memory's mystic band,
Like pilgrim's wither'd wreath of flowers
  Pluck'd in a far-off land.
```

## Contents

[I. DOWN THE RABBIT-HOLE](#chapter-i)  
[II. THE POOL OF TEARS](#chapter-ii)  
[III. A CAUCUS-RACE AND A LONG TALE](#chapter-iii)  
[IV. THE RABBIT SENDS IN A LITTLE BILL](#chapter-iv)  
[V. ADVICE FROM A CATERPILLAR](#chapter-v)  
[VI. PIG AND PEPPER](#chapter-vi)  
[VII. A MAD TEA-PARTY](#chapter-vii)  
[VIII. THE QUEEN'S CROQUET-GROUND](#chapter-viii)  
[IX. THE MOCK TURTLE'S STORY](#chapter-ix)  
[X. THE LOBSTER QUADRILLE](#chapter-x)  
[XI. WHO STOLE THE TARTS?](#chapter-xi)  
[XII. ALICE'S EVIDENCE](#chapter-xii)  
---  
1

![DOWN THE RABBIT-HOLE][4]

## Chapter I  

DOWN THE RABBIT-HOLE

ALICE was beginning to get very tired of  
sitting by her sister on the bank, and of having  
nothing to do: once or twice she had peeped into  
the book her sister was reading, but it had no  
pictures or conversations in it, " and what is

---
7

Alice was not a bit hurt, and she jumped up  
on to her feet in a moment: she looked up,  
but it was all dark overhead; before her was  
another long passage, and the White Rabbit was  
still in sight, hurrying down it. There was  
not a moment to be lost: away went Alice like  
the wind, and was just in time to hear it say, as  
it turned a corner, "Oh my ears and whiskers,  
how late it's getting!" She was close behind  
it when she turned the corner, but the Rabbit  
was no longer to be seen: she found herself in  
a long, low hall, which was lit up by a row of  
lamps hanging from the roof.  
There were doors all around the hall, but they  
were all locked, and when Alice had been all  
the way down one side and up the other, trying  
every door, she walked sadly down the middle,  
wondering how she was ever to get out again.  
Suddenly she came upon a little three-legged  
table, all made of solid glass; there was nothing  
on it but a tiny golden key, and Alice's first  
idea was that this might belong to one of the  

---
13

in a game of croquet she was playing against  
herself, for this curious child was very fond of  
pretending to be two people. “ But it ’s no use  
now,” thought poor Alice, “ to pretend to be two  
people ! Why, there ’s hardly enough of me left  
to make *one* respectable person !”  
&nbsp;&nbsp;&nbsp;&nbsp;Soon her eye fell on a little glass box that  
was lying under the table : she opened it, and  
found in it a very small cake, on which the  
words “ EAT ME” were beautifully marked in  
currants. “ Well, I ’ll eat it,” said Alice, “ and if  
it makes me grow larger, I can reach the key ;  
and if it makes me grow smaller, I can creep  
under the door ; so either way I ’ll get into the  
garden, and I don’t care which happens !”  
&nbsp;&nbsp;&nbsp;&nbsp;She ate a little bit, and said anxiously to  
herself “ Which way ? Which way ?” holding her  
hand on the top of her head to feel which way  
it was growing, and she was quite surprised  
to find that she remained the same size : to be  
sure, this is what generally happens when one  
eats cake, but Alice had got so much into the

---
22  

again". She got up and went to the table to  
measure herself by it, and found that, as nearly  
as she could guess, she was now about two feet  
high, and was going on shrinking rapidly:  
she soon found out that the cause of this was the  
fan she was holding, and she dropped it hastily,  
just in time to save herself from shrinking away  
altogether.  
&nbsp;&nbsp;&nbsp;&nbsp;"That *was* a narrow escape!" said Alice, a  
good deal frightened at the sudden change, but  
very glad to find herself still in existence;" and  
now for the garden!" and she ran with all  
speed back to the little door: but, alas! the  
little door was shut again, and the little golden  
key was lying on the glass table as before," and  
things are worse than ever," thought the poor  
child, "for I never was so small as this before,  
never! And I declare it's too bad, that it is!"  
&nbsp;&nbsp;&nbsp;&nbsp;As she said these words her foot slipped,  
and in another moment, splash! she was up to  
her chin in salt water. Her first idea was that  
she had somehow fallen into the sea," and in  

---
23  

![THE POOL OF TEARS][9]  
that case I can go back by railway," she said  
to herself. (Alice had been to the seaside once  
in her life, and had come to the general con-  
clusion, that wherever you go to on the English  
coast you find a number of bathing machines  
in the sea, some children digging in the sand  
with wooden spades, then a row of lodging  
houses, and behind them a railway station.)  
However, she soon made out that she was in  
the pool of tears which she had wept when she  
was nine feet high.  
&nbsp;&nbsp;&nbsp;&nbsp;"I wish I hadn't cried so much!" said Alice,  
as she swam about, trying to find her way out.  

---
45

![Alice][5]

elbow against the door, and the other arm  
curled round her head. Still she went on growing,  
and, as a last resource, she put one arm  
out of the window, and one foot up the chimney,  
and said to herself, “Now I can do no more,  
whatever happens. What *will* become of me ?”  
&nbsp;&nbsp;&nbsp;&nbsp;Luckily for Alice, the little magic bottle had  
now had its full effect, and she grew no larger:  
still it was very uncomfortable, and, as there  
seemed to be no sort of chance of her ever

---
46

getting out of the room again, no wonder she  
felt unhappy.  
&nbsp;&nbsp;&nbsp;&nbsp;“It was much pleasanter at home,” thought  
poor Alice, “when one wasn’t always growing  
larger and smaller, and being ordered about by  
mice and rabbits. I almost wish I hadn’t gone  
down that rabbit-hole—and yet—and yet—it ’s  
rather curious, you know, this sort of life! I  
do wonder what *can* have happened to me !  
When I used to read fairy-tales, I fancied that  
kind of thing never happened, and now here I  
am in the middle of one ! There ought to be  
a book written about me, that there ought !  
And when I grow up, I’ll write one—but I’m  
grown up now,” she added in a sorrowful tone,  
“at least there’s no room to grow up anymore  
*here*.”  
&nbsp;&nbsp;&nbsp;&nbsp;“ But then,” thought Alice, “ shall I *never*  
get any older than I am now ? That ’ll be a  
comfort, one way—never to be an old woman—  
but then—always to have lessons to learn ! Oh,  
I shouldn’t like *that!*”

---
47

"Oh, you foolish Alice!" she answered her-  
self. "How can you learn lessons in here? Why,  
there's hardly room for you, and no room at all  
for any lesson-books!"  
And so she went on, taking first one side and  
then the other, and making quite a conversation  
of it altogether, but after a few minutes she  
heard a voice outside, and stopped to listen.  
"Mary Ann! Mary Ann!" said the voice,  
"fetch me my gloves this moment!" Then came  
a little pattering of feet on the stairs. Alice  
knew it was the Rabbit coming to look for her,  
and she trembled till she shook the house, quite  
forgetting that she was now about a thousand  
times as large as the Rabbit, and had no reason  
to be afraid of it.  
Presently the Rabbit came up to the door,  
and tried to open it, but as the door opened  
inwards, and Alice's elbow was pressed hard  
against it, that attempt proved a failure. Alice  
heard it say to itself, "Then I'll go round and  
get in at the window."

---
123  

become of me ? They ’re dreadfully fond of  
beheading people here : the great wonder is,  
that there’s any one left alive !”  
She was looking about for some way of  
escape, and wondering whether she could get  
away without being seen, when she noticed a  
curious appearance in the air : it puzzled her  
very much at first, but after watching it a  
minute or two she made it out to be a grin,  
and she said to herself, “ It’s the Cheshire Cat:  
now I shall have somebody to talk to.”  
“ How are you getting on ?” said the Cat,  
as soon as there was mouth enough for it to  
speak with.  
Alice waited till the eyes appeared, and then  
nodded. “ It ’s no use speaking to it,” she  
thought, “ till its ears have come, or at least  
one of them.” In another minute the whole  
head appeared, and then Alice put down her  
flamingo, and began an account of the game,  
feeling very glad she had some one to listen to  
her. The Cat seemed to think that there was  

---
125

&nbsp;&nbsp;&nbsp;&nbsp;“Who *are* you talking to?” said the King,  
coming up to Alice, and looking at the Cat’s  
head with great curiosity.  
&nbsp;&nbsp;&nbsp;&nbsp;“It’s a friend of mine—a Cheshire Cat,” said  
Alice: “allow me to introduce it.”  
&nbsp;&nbsp;&nbsp;&nbsp;“I don’t like the look of it at all,” said the  
King: “ however, it may kiss my hand if it  
likes.”  
&nbsp;&nbsp;&nbsp;&nbsp;“I’d rather not,” the Cat remarked.  
&nbsp;&nbsp;&nbsp;&nbsp;“Don’t be impertinent,” said the King, “and  
don’t look at me like that!” He got behind  
Alice as he spoke.  
&nbsp;&nbsp;&nbsp;&nbsp;“A cat may look at a king,” said Alice.  
“I’ve read that in some book, but I don’t re-  
member where.”  
&nbsp;&nbsp;&nbsp;&nbsp;“Well, it must be removed,” said the King  
very decidedly, and he called the Queen, who  
was passing at the moment, “My dear! I wish  
you would have this cat removed!”  
&nbsp;&nbsp;&nbsp;&nbsp;The Queen had only one way of settling all  
difficulties, great or small. “Off with his head!”  
she said without even looking round.  

---
144

“I never heard of ‘Uglification,’ ” Alice ven-  
tured to say. “What is it ?”  
The Gryphon lifted up both its paws in sur-  
prise. “Never heard of uglifying !” it exclaimed.  
“You know what to beautify is, I suppose ?”  
“Yes,” said Alice, doubtfully : “ it means—  
to—make—anything—prettier.”  
“Well then,” the Gryphon went on, “if you  
don ’t know what to uglify is, you *are* a  
simpleton.”  
Alice did not feel encouraged to ask any  
more questions about it, so she turned to the  
Mock Turtle, and said “What else had you to  
learn?”  
“Well, there was Mystery,” the Mock Turtle  
replied, counting off the subjects on his flappers,—  
“Mystery, ancient and modern, with Seaography:  
then Drawling—the Drawling-master was an old  
conger-eel, that used to come once a week: *he*  
taught us Drawling, Stretching, and Fainting in  
Coils.”  
“What *was* that like?” said Alice.  

---
148

(Alice  began to say “ I once tasted—” but  
checked herself hastily, and said, “No, never”)—  
“so you can have no idea what a delightful  
thing a Lobster Quadrille is!”  
“No, indeed,” said Alice. “What sort of a  
dance is it?”  
“Why,” said the Gryphon, “you first form  
into a line along the seashore—”  
“Two lines!” cried the Mock Turtle. “Seals,  
turtles, salmon, and so on: then, when you’ve  
cleared all the jelly-fish out of the way—”  
“*That* generally takes some time,” inter-  
rupted the Gryphon.  
“—you advance twice—”  
“Each with a lobster as a partner!” cried the  
Gryphon.  
“Of course,” the Mock Turtle said : “advance  
twice, set to partners—”  
“—change lobsters, and retire in same order,”  
continued the Gryphon.  
“Then, you know,” the Mock Turtle went  
on, “you throw the—"  

---
156

  So Alice began telling them her adventures  
from the time when she first saw the White  
Rabbit: she was a little nervous about it just at  
first, the two creatures got so close to her, one  
on each side, and opened their eyes and mouths  
so _very_ wide, but she gained courage as she  
went on. Her listeners were perfectly quiet  
till she got to the part about her repeating  
“_You are old, Father William,_” to the Cater-  
pillar, and the words all coming different, and  
then the Mock Turtle drew a long breath, and  
said, “That’s very curious.”  
  “It’s all about as curious as it can be,” said  
the Gryphon.  
  “It all came different!” the Mock Turtle  
repeated thoughtfully. “I should like to hear  
her try and repeat something now. Tell her  
to begin.” He looked at the Gryphon as if he  
thought it had some kind of authority over  
Alice.  
  “Stand up and repeat ‘’_Tis the voice of the_  
_sluggard,_’” said the Gryphon.

---
182

“Please your Majesty,” said the Knave,  
“I didn’t write it, and they can’t prove I did:  
there ’s no name signed at the end.”  
“If you didn’t sign it,” said the King,“ that  
only makes the matter worse. You must have  
meant some mischief, or else you ’d have signed  
your name like an honest man.”  
There was a general clapping of hands at  
this : it was the first really clever thing the  
King had said that day.  
“That *proves* his guilt,” said the Queen.  
“It proves nothing of the sort !” said  
Alice. “ Why, you don’t even know what they’re  
about !”  
“Read them,” said the King.  
The White Rabbit put on his spectacles.  
“Where shall I begin, please your Majesty ?”  
he asked.  
“Begin at the beginning,” the King said,  
gravely, “ and go on till you come to the end:  
then stop.”  
These were the verses the White Rabbit read: —

[1]: https://pbs.twimg.com/media/EAts-3NXYAQ-qrS.png
[2]: https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf
[3]: https://www.gutenberg.org/files/19778/19778-h/images/frontipiece.jpg
[4]: https://www.gutenberg.org/files/19778/19778-h/images/p001.png
[5]: https://www.gutenberg.org/files/19778/19778-h/images/p042.png
[9]: https://www.gutenberg.org/files/19778/19778-h/images/p021.png
